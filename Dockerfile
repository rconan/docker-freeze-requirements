FROM python:3.6.2-alpine3.6

COPY requirements/frozen.txt /requirements.txt

RUN pip install -r /requirements.txt

WORKDIR /code
ENTRYPOINT ["freeze-requirements"]
